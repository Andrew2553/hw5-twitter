

class Card {
  constructor(post) {
    this.post = post;
  }

  render() {
    const card = document.createElement("div");
    card.classList.add("card");

    const header = document.createElement("h2");
    header.classList.add("card-header");
    header.textContent = this.post.title;

    const text = document.createElement("p");
    text.classList.add("card-text");
    text.textContent = this.post.body;

    const user = document.createElement("p");
    user.classList.add("card-user");
    user.textContent = `Posted by: ${this.post.name} (${this.post.email})`;

    const deleteButton = document.createElement("button");
    deleteButton.classList.add("delete-button");
    deleteButton.textContent = "Delete";
    deleteButton.addEventListener("click", async () => {
      await deletePost(this.post.id);
      card.remove();
    });

    card.appendChild(header);
    card.appendChild(text);
    card.appendChild(user);
    card.appendChild(deleteButton);
    
    return card;
  }
}

async function fetchPosts() {
  const response = await fetch("https://ajax.test-danit.com/api/json/posts");
  const posts = await response.json();
  return posts;
}

async function fetchUsers() {
  const response = await fetch("https://ajax.test-danit.com/api/json/users");
  const users = await response.json();
  return users;
}

async function deletePost(postId) {
  await fetch(`https://ajax.test-danit.com/api/json/posts/${postId}`, {
    method: "DELETE",
  });
}

async function createPost(title, body, userId) {
  const response = await fetch("https://ajax.test-danit.com/api/json/posts", {
    method: "POST",
    headers: {
      "Content-Type": "application/json",
    },
    body: JSON.stringify({
      title,
      body,
      userId,
    }),
  });

  const newPost = await response.json();
  return newPost;
}

document.addEventListener("DOMContentLoaded", async () => {
  const posts = await fetchPosts();
  const users = await fetchUsers(); // Отримання списку користувачів
  let currentUser = users.find((user) => user.id === 1);

  const newsFeed = document.getElementById("news-feed");

  for (const post of posts.reverse()) {
    const user = users.find((user) => user.id === post.userId); // Знаходимо відповідного користувача за ID
    const card = new Card({ ...post, name: user.name, email: user.email });
    newsFeed.appendChild(card.render());
  }

  const addPostButton = document.getElementById("add-post-button");
  const modal = document.getElementById("modal");
  const closeButton = document.querySelector(".close-button");
  const submitPostButton = document.getElementById("submit-post-button");
  const postTitleInput = document.getElementById("post-title");
  const postTextInput = document.getElementById("post-text");

  addPostButton.addEventListener("click", () => {
    modal.style.display = "block";
  });

  closeButton.addEventListener("click", () => {
    modal.style.display = "none";
  });

  submitPostButton.addEventListener("click", async () => {
    const title = postTitleInput.value;
    const body = postTextInput.value;

    if (title && body && currentUser) {
      const newPost = await createPost(title, body, currentUser.id);
      const card = new Card({
        ...newPost,
        name: currentUser.name,
        email: currentUser.email,
      });
      newsFeed.insertBefore(card.render(), newsFeed.firstChild);
      modal.style.display = "none";
      postTitleInput.value = "";
      postTextInput.value = "";
    }
  });
});


